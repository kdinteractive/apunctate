package com.cide.interactive.apunctate;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class ApunctateService extends Service {
    private static final String TAG = "ApunctateService";
    public final static String LISTEN_TO_SERVER = "action.apunctateService.start";
    public final static String STOP_LISTEN_TO_SERVER = "action.apunctateService.stop";
    public final static String APUNTATE_BROADCAST  = "com.cide.interactive.apunctate.APUNTATE";
    final static int TWENTY_SEC_IN_MILLIS = 20 * 1000;
    SharedPreferences sharedPreferences;

    private Thread serviceThread;

    public ApunctateService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null) {
            if (intent.getAction().equals(LISTEN_TO_SERVER)) {
                startProcessMonic();
            }
            if (intent.getAction().equals(STOP_LISTEN_TO_SERVER)) {
                if (serviceThread != null) {
                    serviceThread.interrupt();
                    stopForeground(true);
                    serviceThread = null;
                }
            }
        }

        return START_STICKY;
    }

    private void startProcessMonic() {


        if (serviceThread == null) {


            Log.d(TAG, "ApunctateService is null");
            String chanelId = "ApunctateService";
            createNotificationChannel(chanelId, "Apunctate Service", false, false, NotificationManager.IMPORTANCE_MIN);
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, getPackageManager().getLaunchIntentForPackage(getPackageName()), 0);

            Notification.Builder b = new Notification.Builder(getApplicationContext(), chanelId);
            b.setOngoing(true)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("Apunctate service is working")
                    .setSmallIcon(R.drawable.ic_baseline_settings_ethernet_24)
                    .setColor(getResources().getColor(R.color.kurio_blue, null))
                    .setAutoCancel(false)
                    .setCategory(Notification.CATEGORY_SYSTEM);

            b.setContentIntent(pendingIntent);
            startForeground(1358, b.build());

            //start TimeControl thread
            serviceThread = new Thread(new ApunctateControlThread());
            serviceThread.setName("apunctate service");
            serviceThread.start();
            Log.d(TAG, "ApunctateService has started");
        } else {
            Log.d(TAG, "ApunctateService is already running, no need to start");
        }
    }

    private void createNotificationChannel(String chanelId, String channelName, boolean deletable, boolean vibration, int importance) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        NotificationChannel notificationChannel = new NotificationChannel(chanelId, channelName, importance);
        notificationChannel.enableLights(false);
        notificationChannel.setImportance(importance);
        notificationChannel.enableVibration(vibration);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_SECRET);
        notificationChannel.setVibrationPattern(new long[]{300, 300, 300});
        notificationChannel.setBypassDnd(true);
        notificationChannel.setShowBadge(false);
        notificationManager.createNotificationChannel(notificationChannel);
    }

    class ApunctateControlThread implements Runnable {

        @Override
        public void run() {
            try {

                while (!Thread.currentThread().isInterrupted()) {
                    Log.d(TAG, "it's working ");
                    checkForNotificationFromServer();
                    Thread.sleep(5000); // pause 20 seconds
                }
            } catch (InterruptedException consumed) {
                Log.d(TAG, "thread has been stopped : ", consumed);
                //Thread.currentThread.interrupt();
            }
        }
    }

    public boolean checkForNotificationFromServer() {
        final boolean isMessage = false;
        if (Connectivity.isConnected(this)) {
            new AsyncTask<String, File, String>() {
                @Override
                protected String doInBackground(String... strings) {
                    String textOrUrl="";

                    try {
                        Log.d(TAG,"check messages");
                        URL url = new URL("https://kdtablets.com/mexico/read_message2.php?serial=" + Utils.getSN());
                        URLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                        textOrUrl = Utils.convertStreamToString(in);
                    } catch (Exception e) {
                        Log.e(TAG,e.getMessage());
                    }
                    return textOrUrl;
                }

                @Override
                protected void onPostExecute(String message) {
                    if (message==null || message.isEmpty()){
                        return;
                    }
                    LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
                    Intent local = new Intent();
                    local.putExtra("message",message);
                    local.setAction(APUNTATE_BROADCAST);
                    localBroadcastManager.sendBroadcast(local);
                }
            }.execute();
        }
        return isMessage;
    }
}