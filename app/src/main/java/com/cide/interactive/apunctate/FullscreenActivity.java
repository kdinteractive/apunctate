package com.cide.interactive.apunctate;

import android.annotation.SuppressLint;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ServiceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.telephony.DctConstants;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.StandardWatchEventKinds;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenActivity extends AppCompatActivity {

    private final static String TAG = "Apunctate";
    Dialog dialog;
    AlertDialog alertDialog;
    WebView webView;
    ConstraintLayout clMainActivity;
    WeakReference<Context> contextWeakReference;
    String message = "";
    BroadcastReceiver updateUIReciver;
    LocalBroadcastManager localBroadcastManager;

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);

        webView = findViewById(R.id.wvApunctate);
        clMainActivity = findViewById(R.id.clMainActivity);
    }

    @Override
    public void onBackPressed() {
        if(webView.canGoBack()) {
            webView.goBack();
        }
    }

    @Override
    protected void onResume() {
        contextWeakReference = new WeakReference<>(getApplicationContext());
        Intent intentProcessMonitorService = new Intent(getApplicationContext(), ApunctateService.class);
        intentProcessMonitorService.setAction(ApunctateService.LISTEN_TO_SERVER);
        getApplicationContext().startService(intentProcessMonitorService);
        isNewMessage(getIntent().getStringExtra("message"));

        initWebview();

        IntentFilter filter = new IntentFilter();
        filter.addAction(ApunctateService.APUNTATE_BROADCAST);
        localBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        updateUIReciver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                isNewMessage(intent.getStringExtra("message"));
            }
        };
        localBroadcastManager.registerReceiver(updateUIReciver,filter);
        super.onResume();
    }

    @Override
    protected void onPause() {
        dismissDialogs();
        localBroadcastManager.unregisterReceiver(updateUIReciver);
        super.onPause();
    }

    public void initWebview() {
        String url = "https://kdplanet.com/apuntate/latests/";
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(false);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });

        if (webView.getUrl() == null || webView.getUrl() != null && !webView.getUrl().equals(url)) {
            try {
                webView.loadUrl(url);
            } catch (Exception e) {
                Log.e("Kurio", e.getMessage());
            }
        }
    }

    public void dismissDialogs() {
        if (webView != null) {
            webView.destroy();
        }
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public boolean isNewMessage(String message) {
        if (message != null && !message.equals(this.message)) {
            this.message = message;
            String[] token = message.split(";");
            String type = token[0];
            String value = token[1].replace("\n", "");

            Intent intention = null;
            switch (type) {
                case "web_fullscreen":
                    // intention = new Intent(getApplicationContext(), NotificationFromServer.class);
                    displayWebViewDialog(value, true);
                    break;
                case "video":
                    intention = new Intent(FullscreenActivity.this, PlayerViewDemoActivity.class);
                    intention.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    break;
                case "text":
                    displayMessage(value);
                    break;
                case "kdtext":
                    displayKDMessage(value);
                    break;
                case "image":
                    new ShowImageTask().execute(value);
                    break;
                case "web":
                    displayWebViewDialog(value, false);
                    break;
            }

            if (intention != null) {
                intention.putExtra("message", value);
                startActivity(intention);
            }
            return true;
        }
        return false;
    }

    public void displayMessage(String message) {
        alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(message);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void displayKDMessage(String message) {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog);

        ImageView image = dialog.findViewById(R.id.ImageView01);
        image.setImageResource(R.drawable.logo);

        TextView text = dialog.findViewById(R.id.TextView01);
        text.setText(message);

        Button dialogButton = dialog.findViewById(R.id.Button01);

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
//                context.onBackPressed();
            }
        });

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public void displayWebViewDialog(String url, boolean fullScreen) {
        webView = new WebView(this);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(false);

        if (!url.startsWith("http")) {
            url = "https://" + url;
        }

        webView.setWebViewClient(new WebViewClient());
        try {
            webView.loadUrl(url);
        } catch (Exception e) {
            Log.e("Kurio", e.getMessage());
        }
        Dialog builder;
        if (fullScreen) {
            builder = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen); //android.R.style.Theme
        } else {
            builder = new Dialog(this); //android.R.style.Theme
        }

        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        builder.addContentView(webView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();
    }


    private class ShowImageTask extends AsyncTask<String, Void, Bitmap> {

        public ShowImageTask() {
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            ImageView imageView = new ImageView(FullscreenActivity.this);
            imageView.setImageBitmap(result);


            Dialog builder = new Dialog(FullscreenActivity.this);
            builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
            builder.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    //nothing;
                }
            });


            builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            builder.show();

        }

    }
}